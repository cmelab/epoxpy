from freud import LinkCell


def get_sphere_tags(snapshot, sphere_size):
    my_neighbor_query = LinkCell.from_system(snapshot)
    return my_neighbor_query.query([0, 0, 0],
                                   query_args={'r_max': sphere_size}).toNeighborList()
